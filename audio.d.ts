interface AudioSounds {
    pressed: string;
    hit: string;
    reached: string;
}
interface Audio {
    default: AudioSounds;
    kirby: AudioSounds;
}
declare const audio: Audio;
export default audio;
//# sourceMappingURL=audio.d.ts.map