declare function Runner(outerContainerId: any, shadowRoot: any, onAchievement: (() => void) | undefined, spriteDef: any, opt_config: any): any;
declare namespace Runner {
    var config: {
        ACCELERATION: number;
        BG_CLOUD_SPEED: number;
        BOTTOM_PAD: number;
        CLEAR_TIME: number;
        CLOUD_FREQUENCY: number;
        GAMEOVER_CLEAR_TIME: number;
        GAP_COEFFICIENT: number;
        GRAVITY: number;
        INITIAL_JUMP_VELOCITY: number;
        INVERT_FADE_DURATION: number;
        INVERT_DISTANCE: number;
        MAX_BLINK_COUNT: number;
        MAX_CLOUDS: number;
        MAX_OBSTACLE_LENGTH: number;
        MAX_OBSTACLE_DUPLICATION: number;
        MAX_SPEED: number;
        MIN_JUMP_HEIGHT: number;
        MOBILE_SPEED_COEFFICIENT: number;
        RESOURCE_TEMPLATE_ID: string;
        SPEED: number;
        SPEED_DROP_COEFFICIENT: number;
        ARCADE_MODE_INITIAL_TOP_POSITION: number;
        ARCADE_MODE_TOP_POSITION_PERCENT: number;
    };
    var defaultDimensions: {
        WIDTH: number;
        HEIGHT: number;
    };
    var classes: {
        ARCADE_MODE: string;
        CANVAS: string;
        CONTAINER: string;
        CRASHED: string;
        ICON: string;
        INVERTED: string;
        SNACKBAR: string;
        SNACKBAR_SHOW: string;
        TOUCH_CONTROLLER: string;
    };
    var spriteDefinition: {
        LDPI: {
            CACTUS_LARGE: {
                x: number;
                y: number;
            };
            CACTUS_SMALL: {
                x: number;
                y: number;
            };
            CLOUD: {
                x: number;
                y: number;
            };
            HORIZON: {
                x: number;
                y: number;
            };
            MOON: {
                x: number;
                y: number;
            };
            PTERODACTYL: {
                x: number;
                y: number;
            };
            RESTART: {
                x: number;
                y: number;
            };
            TEXT_SPRITE: {
                x: number;
                y: number;
            };
            TREX: {
                x: number;
                y: number;
            };
            STAR: {
                x: number;
                y: number;
            };
        };
        HDPI: {
            CACTUS_LARGE: {
                x: number;
                y: number;
            };
            CACTUS_SMALL: {
                x: number;
                y: number;
            };
            CLOUD: {
                x: number;
                y: number;
            };
            HORIZON: {
                x: number;
                y: number;
            };
            MOON: {
                x: number;
                y: number;
            };
            PTERODACTYL: {
                x: number;
                y: number;
            };
            RESTART: {
                x: number;
                y: number;
            };
            TEXT_SPRITE: {
                x: number;
                y: number;
            };
            TREX: {
                x: number;
                y: number;
            };
            STAR: {
                x: number;
                y: number;
            };
        };
    };
    var sounds: {
        BUTTON_PRESS: string;
        HIT: string;
        SCORE: string;
    };
    var keycodes: {
        JUMP: {
            '38': number;
            '32': number;
        };
        DUCK: {
            '40': number;
        };
        RESTART: {
            '13': number;
        };
    };
    var events: {
        ANIM_END: string;
        CLICK: string;
        KEYDOWN: string;
        KEYUP: string;
        MOUSEDOWN: string;
        MOUSEUP: string;
        RESIZE: string;
        TOUCHEND: string;
        TOUCHSTART: string;
        VISIBILITY: string;
        BLUR: string;
        FOCUS: string;
        LOAD: string;
    };
    var updateCanvasScaling: (canvas: any, opt_width: any, opt_height: any) => boolean;
}
export default Runner;
//# sourceMappingURL=index.d.ts.map