import {LitElement, html} from 'lit';
import {customElement, property} from 'lit/decorators.js';
import styles from './runner/styles';
import audio from './audio';
// @ts-ignore:next-line
import Runner from './runner';

export const DEFAULTS = {
  SPRITE_100: 'assets/default_100_percent/100-offline-sprite.png',
  SPRITE_200: 'assets/default_200_percent/200-offline-sprite.png'
};

@customElement('t-rex-runner')
export class TRexRunner extends LitElement {
  static override styles = styles;

  _runner: any = null;

  @property({ reflect: true })
  spriteSheet1x: string = DEFAULTS.SPRITE_100;

  @property({ reflect: true })
  spriteSheet2x: string = DEFAULTS.SPRITE_200;

  @property({ reflect: true })
  soundEffects: 'default' | 'kirby' = 'default';

  override firstUpdated(): void {
    // @ts-ignore:next-line
    this._runner = new Runner('.interstitial-wrapper', this.shadowRoot, this._onAchievement.bind(this));
  }

  override updated(changedProperties: any): void {
    changedProperties.forEach((prev: String, propertyName: String) => {
      if (propertyName === 'soundEffects' && prev) {
        this._runner.loadSounds();
      }
    }) 
  }

  override disconnectedCallback(): void {
    if (this._runner) {
      this._runner.stopListening();
    }
  }

  private _onAchievement(): void {
    const achievementEvent = new CustomEvent('achievement', {
      detail: { message: 'achievement message' },
      bubbles: true,
      composed: true 
    });
    this.dispatchEvent(achievementEvent);

  }

  override render() {
    return html`
      <div id="main-frame-error" class="interstitial-wrapper">
        <div id="main-content">
          <div class="icon icon-offline"></div>
        </div>
        <div id="offline-resources">
          <img id="offline-resources-1x" src=${this.spriteSheet1x}>
          <img id="offline-resources-2x" src=${this.spriteSheet2x}>
          <div id="audio-resources">
            <audio id="offline-sound-press" src=${audio[this.soundEffects].pressed}></audio>
            <audio id="offline-sound-hit" src=${audio[this.soundEffects].hit}></audio>
            <audio id="offline-sound-reached" src=${audio[this.soundEffects].reached}></audio>
          </div>
        </div>
      </div>
      <slot></slot>
    `;
  }
}

declare global {
  interface HTMLElementTagNameMap {
    't-rex-runner': TRexRunner;
  }
}
